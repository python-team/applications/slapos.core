SHELL=/bin/sh
PATH=/usr/bin:/usr/sbin:/sbin:/bin
MAILTO=root

# Run "Installation/Destruction of Software Releases" and "Deploy/Start/Stop Partitions" once per minute
* * * * *       root    /usr/bin/slapos node software --verbose --logfile=/var/log/slapos/slapos-node-software.log --pidfile=/var/run/slapos-node-software.pid > /dev/null 2>&1
* * * * *       root    /usr/bin/slapos node instance --promise-timeout 20 --verbose --log-file=/var/log/slapos/slapos-node-instance.log --pidfile=/var/run/slapos-node-instance.pid > /dev/null 2>&1

# Run "Destroy Partitions to be destroyed" once per hour
0 * * * *       root    /usr/bin/slapos node report --maximal_delay=3600 --verbose --logfile=/var/log/slapos/slapos-node-report.log > /dev/null 2>&1

# Run "Check/add IPs and so on" once per hour
0 * * * *       root    /usr/bin/slapos node format >> /var/log/slapos/slapos-node-format.log 2>&1

# Run "Booting" on every system start
@reboot         root    /usr/bin/slapos node boot >> /var/log/slapos/slapos-node-boot.log 2>&1

# Run "Collect" once a minute
* * * * *       root    /usr/bin/slapos node collect >> /var/log/slapos/slapos-node-collect.log 2>&1
